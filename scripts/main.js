const tokenCallback = (errorDesc, token) => {
};

(() => {
    const app = new Msal.UserAgentApplication('1742d5b1-d36d-4727-9e71-48dda8948163', null, tokenCallback);

    document.querySelector('#signIn')
        .addEventListener('click', async () => {
            const token = await app.loginPopup(['user.read']);
            console.log(token);
        });
})();
